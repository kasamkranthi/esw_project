const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const RecordSchema = new Schema({
    grideye:{
        type:String,
        required:true
    },
    time: {
        type:Date,
        required:true
    },
    pir:{
        type:String
    }
    
});

module.exports = Realtime = mongoose.model('Current',RecordSchema);