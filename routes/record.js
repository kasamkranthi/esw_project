const express = require('express');
const router = express.Router();
const Realtime = require('../models/realtime')
const Zero = require('../models/zero');
const One = require('../models/one');
const Two = require('../models/two');
const Three=require('../models/three');

const CryptoJS = require("crypto-js");
const AESKey='2B7E151628AED2A6ABF7158809CF4F3C';

router.get('/',async(req,res)=>{
    try{ //console.log(new Date(req.query.start),new Date( req.query.end));
        let zero=await Zero.find({}).lean();
        let a=[]
        zero.forEach(element => {
            
            b={
                "grideye":element.grideye,
                "time":element.time
            }
            a.push(b)
        });
        return res.status(200).json(a);
    }
    catch(e){
        res.status(400).json({msg:e.message});
    }

    
});



router.get('/oneentry',async(req,res)=>{
    try{ //console.log(new Date(req.query.start),new Date( req.query.end));
        let zero=await Realtime.find({}).lean().sort({"time":-1}).limit(1);
        let a=[]
        zero.forEach(element => {
            
            b={
                "grideye":element.grideye,
                "time":element.time,
                "pirvalue":element.pir
            }
            a.push(b)
        });
        return res.status(200).json(a[0]);
    }
    catch(e){
        res.status(400).json({msg:e.message});
    }

    
});

router.post('/realtime',async(req,res)=>{
    
    var esp8266_msg = req.body.payLoad;
    var esp8266_iv = req.body.iv4;
    var sourceshaVal = req.body.shaVal;

    
    var clientshaVal= CryptoJS.SHA256(req.body.payLoad);

    console.log(sourceshaVal);
    console.log(clientshaVal);
    if(clientshaVal != sourceshaVal){
        console.log("Hash Check Succesful");
    }
    else{
        console.log("Hash Check Un-Succesful");

    }


    
    var plain_iv =  Buffer.from( esp8266_iv , 'base64').toString('hex');
    var iv = CryptoJS.enc.Hex.parse( plain_iv );
    var key= CryptoJS.enc.Hex.parse( AESKey );

    var bytes  = CryptoJS.AES.decrypt( esp8266_msg, key , { iv: iv} );
    var plaintext = bytes.toString(CryptoJS.enc.Base64);
    var decoded_b64msg =  Buffer.from(plaintext , 'base64').toString('ascii');
    var decoded_msg =     Buffer.from( decoded_b64msg , 'base64').toString('ascii');

    var body = JSON.parse(decoded_msg);
    console.log(body)
    
    
    
    const newRecord = new Realtime({
        time:Date.now(),
        grideye:body.grideye,
        pir:body.pir
        
    });

    newRecord.save().then(rec => res.json("record created succesfully"));

    
});



router.get('/one',async(req,res)=>{
    try{ //console.log(new Date(req.query.start),new Date( req.query.end));
        let zero=await One.find({}).lean();
        let a=[]
        zero.forEach(element => {
            
            b={
                "grideye":element.grideye,
                "time":element.time
            }
            a.push(b)
        });
        return res.status(200).json(a);
    }
    catch(e){
        res.status(400).json({msg:e.message});
    }

    
});

router.get('/two',async(req,res)=>{
    try{ //console.log(new Date(req.query.start),new Date( req.query.end));
        let zero=await Two.find({}).lean();
        let a=[]
        zero.forEach(element => {
            
            b={
                "grideye":element.grideye,
                "time":element.time
            }
            a.push(b)
        });
        return res.status(200).json(a);
    }
    catch(e){
        res.status(400).json({msg:e.message});
    }

    
});


router.get('/three',async(req,res)=>{
    try{ //console.log(new Date(req.query.start),new Date( req.query.end));
        let zero=await Three.find({}).lean();
        let a=[]
        zero.forEach(element => {
            
            b={
                "grideye":element.grideye,
                "time":element.time
            }
            a.push(b)
        });
        return res.status(200).json(a);
    }
    catch(e){
        res.status(400).json({msg:e.message});
    }

    
});

router.post('/',(req,res) => {
    const newRecord = new Record({
        time:Date.now(),
        grideye:req.body.grideye,
        people:req.body.people
        
    });

    newRecord.save().then(rec => res.json("record created succesfully"));
});

module.exports = router;



/* temp: Schema.Types.Decimal128,
    humidity: Schema.Types.Decimal128,
    pm25: Schema.Types.Decimal128,
    pm10: Schema.Types.Decimal128,
    co2: Schema.Types.Decimal128,
    tvoc: Schema.Types.Decimal128,
    eco2: Schema.Types.Decimal128,
    h2: Schema.Types.Decimal128, */